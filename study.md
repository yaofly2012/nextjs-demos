# Automatic code splitting
1. 每个`import`引入的模块都会打包成bundle，
2. 怎么理解“served with each page”？页面也会打入bundle? 会的

# 内置对CSS支持
1. styled-jsx 得好好看看

# 内置组件
1. Link
2. Head

# getInitialProps
1. 方法如何判断是客户端执行还是服务端执行呢？
根据函数的实参，有些只有在Server端有值，有些属性只有在Client有值。
2. getInitialProps执行的原理
- 首次后端执行，并且函数返回值会被JSON化喷到页面里，并赋值给全局变量`__NEXT_DATA__`里，所以返回值必须是合法的JSON类型对象。
- 使用`Link`进行客户端路由时会在客户端执行

## 实参
## 返回值
1. 无状态页面组件，通过参数；
2. 有状态页面组件通过`props`属性访问。