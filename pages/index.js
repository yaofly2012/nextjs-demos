
import cowsay from 'cowsay-browser';
import Head from 'next/head'
import React from 'react'

async function foo() {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve({userAgent: 'haha'})
        }, 0)
    });
}

export default class Index extends React.Component {
    static async getInitialProps({ req }) { // 有req属性表示服务端渲染，否则是后端渲染？
        //const userAgent = req ? req.headers['user-agent'] : navigator.userAgent
        const props = await foo();
        return props
    }

    render() {
        return (
            <div>
                Hello {this.props.userAgent}
            </div>
        )
    }
}


